# LMC-Vision

LMC Vision repository for storing config, documentation and other information for the LMC Vision setup.

## Content

- [Overview](./Documentation/Overview.md)
- [Positions](./Documentation/Positions.md)
- [Terminology](./Documentation/Terminology.md)

For any updates, please provide a Pull Request, or email details to anelson@aliasit.net