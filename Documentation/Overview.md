# Overview

Overview of the LMC Vision setup

![Connectivity Overview](./assets/Connectivity-Overview.png?raw=true "Connectivity Overview")

> Until the equipment is moved into the vision room, the above diagram is not accurate.